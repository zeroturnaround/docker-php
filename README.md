# README #

Dockerfile for PHP 5.6 and extensions:

* ctype
* mysql
* curl
* memcache

# Usage #

`docker run zeroturnaround/php php --info`

Running WordPress with docker-compose.yml:

```
version: '2'
services:
    web:
        image: zeroturnaround/php
        command: php -S 0.0.0.0:8000 -t /code/public_html
        ports:
            - "8000:8000"
        volumes:
            - .:/code
```

 
