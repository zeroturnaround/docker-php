FROM alpine:3.3
RUN apk add -U php php-ctype php-mysql php-curl php-memcache php-json php-zlib && \
    rm -rf /var/cache/apk/*
